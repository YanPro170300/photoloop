#pragma once

#ifndef BMP_HEADER
#define BMP_HEADER

#include <stdint.h>

struct bmp_header_part {
    int16_t signature;
    int32_t file_size;
    int16_t reserved1;
    int16_t reserved2;
    int32_t offset_to_array;
    int32_t info_header_size;
    int32_t width;
    int32_t height;
};

#endif
