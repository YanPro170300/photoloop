#pragma once

#ifndef BLUR
#define BLUR

#include "bmp_info.h"

void blur(struct bmp_info* result_info,
          struct bmp_info* source_info,
          int blur_coef);

#endif
