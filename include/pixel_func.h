#pragma once

#ifndef PIXEL_FUNC
#define PIXEL_FUNC

#include <stdint.h>

typedef uint32_t (*pixel_func)(uint32_t);

uint32_t negate(uint32_t pixel);
uint32_t grayscale(uint32_t pixel);

#endif
