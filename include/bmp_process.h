#pragma once

#ifndef BMP_PROCESS
#define BMP_PROCESS

#include "bmp_info.h"

void bmp_initialize(const char* source_name,
                    const char* result_name,
                    struct bmp_info* source_info,
                    struct bmp_info* result_info);

void bmp_finalize(struct bmp_info* source_info,
                  struct bmp_info* result_info);

#endif
