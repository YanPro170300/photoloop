#include <stdint.h>

uint32_t negate(uint32_t pixel) {
    return (0xFFFFFFFF - pixel) | 0xFF000000;
}

uint32_t grayscale(uint32_t pixel) {
    uint32_t avg = ((pixel & 0x000000FF) +
                    ((pixel & 0x0000FF00) >> 8) +
                    ((pixel & 0x00FF0000) >> 16)) / 3;
    return avg | (avg << 8) | (avg << 16) | 0xFF000000;
}
