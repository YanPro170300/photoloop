#include <stdio.h>
#include <stdlib.h>

#include "apply_matrix_transformation.h"
#include "bmp_info.h"

void custom(struct bmp_info* result_info,
            struct bmp_info* source_info,
            FILE* matrix_file)
{
    int matrix_dim = 1;
    fscanf(matrix_file, "%d", &matrix_dim);
    double coef_sum = 0;
    double** custom_matrix = calloc(matrix_dim, sizeof(double*));

    for (int i = 0; i < matrix_dim; ++i) {
        custom_matrix[i] = calloc(matrix_dim, sizeof(double));
        for (int j = 0; j < matrix_dim; ++j) {
            fscanf(matrix_file, "%lf", &custom_matrix[i][j]);
            coef_sum += custom_matrix[i][j];
        }
    }
    for (int i = 0; i < matrix_dim; ++i) {
        for (int j = 0; j < matrix_dim; ++j) {
            custom_matrix[i][j] /= coef_sum;
        }
    }

    apply_matrix_transformation(result_info->pixel_array,
                                source_info->pixel_array,
                                source_info->width,
                                source_info->height,
                                custom_matrix,
                                matrix_dim);

    for (int i = 0; i < matrix_dim; ++i) {
        free(custom_matrix[i]);
    }
    free(custom_matrix);
}
